import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class BankAccountClient {

    public static void main(String[] args) {
        //BankAccount account = new BankAccount("Vikas", 2000);
        //account.deposit(4000);

        //double updatedBalance = account.checkBalance();
        //System.out.println("Updated balance :: "+updatedBalance);

        // SavingsAccount account = new SavingsAccount("Vikas", 2000);
        // account.deposit(4000);
        // account.setWithdrawalLimit(1000);
        // account.withdraw(2000);

        // double updatedBalance = account.checkBalance();
        // System.out.println("Updated balance :: "+updatedBalance);

        /*
         * public
         * protected
         * default
         * private
         */
        // BankAccount bankAccount = new SavingsAccount("Harish",35_000, 20_000);
        
        // bankAccount.deposit(35000);
        // bankAccount.withdraw(2500);
        // System.out.println("Account balance is "+ bankAccount.checkBalance());
    
        // Scanner scanner = new Scanner(System.in);

        // System.out.println("Please enter the type of account : ");
        // System.out.println("1: Savings Account");
        // System.out.println("2: Current Account");


        // int option = scanner.nextInt();
        // BankAccount bankAccount = null;
        // switch(option){
        //     case 1:
        //        bankAccount = new SavingsAccount("Vikas", 25000, 2000);
        //        break;
        //     case 2:
        //        bankAccount = new CurrentAccount("Ramesh", 25000);
        //        break;   
        //     default:
        //         bankAccount = new CurrentAccount("Ramesh", 25000);
        //         break;
        // }

        // System.out.println("Select the operation");
        // System.out.println("1 -> Deposit");
        // System.out.println("2 -> Withdraw"); 
        // System.out.println("3 -> Check balance");


        // int operation = scanner.nextInt();
        // System.out.println("Please enter the amount");
        // double amount = scanner.nextDouble();

        // switch(operation){
        //     case 1:
        //        bankAccount.deposit(amount);
        //        break;
        //     case 2:
        //        bankAccount.withdraw(amount);
        //        break;   
        //     case 3:
        //        bankAccount.checkBalance();
        //        break;   
        // }
        // scanner.close();;

        SavingsAccount account1 = new SavingsAccount("Harish", 15000, 1000);
        SavingsAccount account2 = new SavingsAccount("Ravish", 15000, 1000);
        SavingsAccount account3 = new SavingsAccount("Satish", 15000, 1000);



        Set<SavingsAccount> accounts = new TreeSet<>(new BankAccountComparatorByBalanceAsc());
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);

        System.out.println("Size of Set :: "+ accounts.size());

        System.out.println(accounts);
        
    }
}
