import java.util.Scanner;

public class PaymentGatewayClient {
    public static void main(String[] args) {
        System.out.println("Please enter your options");
        System.out.println("1 : Google pay");
        System.out.println("2 : Phone pay");
        System.out.println("3 : PayTM");

        PaymentGateway gateway = null;
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        switch(option){
            case 1: 
                gateway = new GooglePay();
                break;
            case 2: 
                gateway = new PhonePay();
                break;
        }

        gateway.transferFunds("Manish", "Vikrant", 25000, "towards loan");
        scanner.close();
    }
}
