public interface PaymentGateway {
    
 void transferFunds(String from, String to, double amount, String notes);

}

class GooglePay implements PaymentGateway{
    
 public void transferFunds(String from, String to, double amount, String notes){

    System.out.printf("Transferring funds from %s to %s of Rs %s with notes \n", from, to, amount, notes);
    System.out.println("You have earned "+ 0.01 * amount + "worth of reward points with Google Pay");
  }
}

class PhonePay implements PaymentGateway{
    public void transferFunds(String from, String to, double amount, String notes){
   
       System.out.printf("Transferring funds from %s to %s of Rs %s with notes \n", from, to, amount, notes);
       System.out.println("You have earned 200 Rs cashback with Phone Pay");
     }
}

class PayTM implements PaymentGateway{
    
    public void transferFunds(String from, String to, double amount, String notes){
   
        System.out.printf("Transferring funds from %s to %s of Rs %s with notes \n", from, to, amount, notes);
        System.out.println("You have earned 500 Rs cashback with PayTM Pay");
      }
}