public class SavingsAccount extends BankAccount {
    
    private double withdrawLimit;
    private boolean isAccountLocked = false;

    public SavingsAccount(String name, double initialDeposit, double withdrawLimit){
        super(name, initialDeposit);
        this.withdrawLimit = withdrawLimit;
    }

    public void setWithdrawalLimit(double amount){
        this.withdrawLimit = amount;
    }

    @Override
    public double withdraw(double amount){
        if(amount > this.withdrawLimit){
            System.out.println("Cannot withdraw more than "+ this.withdrawLimit);
            return 0d;
        } else if (this.accountBalance - amount  > 0){
                this.accountBalance -= amount;
                return amount;
        }
        return 0d;
    }

    //method overloading
    public double withdraw(double amount, String notes){
        if(amount > this.withdrawLimit){
            System.out.println("Cannot withdraw more than "+ this.withdrawLimit);
            return 0d;
        }
        System.out.println("Notes:: "+ notes);
        return this.withdraw(amount);
    }
}
