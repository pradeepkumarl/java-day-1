public abstract class BankAccount implements Comparable<BankAccount>{

    //data is hidden
    private static long accountNumber = 10000;
    String accountHolderName;
    double accountBalance;
    private String branchName;
    

    //behaviour is exposed to the clients
    //access-modifier return-type method-name(arguments ...)
    /*
     * access-modifier - private|default|protected|public
     * return - void|primitive|object
     * 
     */
    public BankAccount(String name, double initialDeposit){
        this.accountHolderName = name;
        this.accountBalance = initialDeposit;
        ++ accountNumber;
    }
    //constructor overloading
    public BankAccount(String name){
        this.accountHolderName = name;
        ++accountNumber;
    }

    public final void deposit(double amount){
        if(amount < 50000){
            this.accountBalance = this.accountBalance + amount;
        } else {
            System.out.println("Amount exceeds 50k. Please provide your PAN number");
        }
    }

    public abstract double withdraw(double amount) throws InsufficientBalanceException;

    public double checkBalance(){
        return this.accountBalance;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountHolderName == null) ? 0 : accountHolderName.hashCode());
        result = prime * result + ((branchName == null) ? 0 : branchName.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BankAccount other = (BankAccount) obj;
        if (accountHolderName == null) {
            if (other.accountHolderName != null)
                return false;
        } else if (!accountHolderName.equals(other.accountHolderName))
            return false;
        if (branchName == null) {
            if (other.branchName != null)
                return false;
        } else if (!branchName.equals(other.branchName))
            return false;
        return true;
    }

    public int compareTo(BankAccount bankAccount){
        /*
            If the current object is greater -> return positive number
            If the current object is less -> return negative number
            If the current object is same -> return 0
         */ 
        return this.accountHolderName.compareTo(bankAccount.accountHolderName);
    }

    @Override
    public String toString() {
        return "BankAccount [accountBalance=" + accountBalance + ", accountHolderName=" + accountHolderName
                + ", branchName=" + branchName + "]";
    }

}