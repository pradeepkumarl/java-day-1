import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterDemo {
    public static void main(String[] args) {
        try(FileWriter writer = new FileWriter(new File("D:\\data.txt"))) {

            writer.write("hello world from File writer");
        } catch(IOException exception){
            System.out.println("Exception :: "+ exception.getMessage());
        }
        

    }
}
