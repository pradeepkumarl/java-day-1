public class CurrentAccount extends BankAccount {
    
    private final double MIN_ACCOUNT_BALANCE = 25_000;

    public CurrentAccount(String name, double initialDeposit){
        super(name, initialDeposit);
    }


    @Override
    public double withdraw(double amount)throws InsufficientBalanceException{
     if (this.accountBalance - amount  > MIN_ACCOUNT_BALANCE){
                this.accountBalance -= amount;
                return amount;
        }
         throw new InsufficientBalanceException("Insuffiennt balance");
    }

    //method overloading
    public double withdraw(double amount, String notes) throws InsufficientBalanceException{
        System.out.println("Notes:: "+ notes);
        return this.withdraw(amount);
    }

    

}
