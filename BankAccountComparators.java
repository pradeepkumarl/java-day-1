import java.util.Comparator;

class BankAccountComparatorByBalanceAsc implements Comparator<BankAccount>{
    @Override
    public int compare(BankAccount bankAccount1, BankAccount bankAccount2) {
        return (int)(bankAccount1.accountBalance - bankAccount2.accountBalance);
    }
}

class BankAccountComparatorByBalanceDesc implements Comparator<BankAccount>{
    @Override
    public int compare(BankAccount bankAccount1, BankAccount bankAccount2) {
        return (int)(bankAccount2.accountBalance - bankAccount1.accountBalance);
    }
}

class BankAccountComparatorByNameAsc implements Comparator<BankAccount>{
    @Override
    public int compare(BankAccount bankAccount1, BankAccount bankAccount2) {
        return bankAccount1.accountHolderName.compareTo(bankAccount2.accountHolderName);
    }
}

class BankAccountComparatorByNameDesc implements Comparator<BankAccount>{
    @Override
    public int compare(BankAccount bankAccount1, BankAccount bankAccount2) {
        return bankAccount2.accountHolderName.compareTo(bankAccount1.accountHolderName);
    }
}